import React from "react";

const About = () => {
  return (
    <div className="wrap">
      <h1 className="title">Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <ol>
        <li>
          <b>Nama :</b> Mohammad Nanda Rizky Dhiya'ulhaq
        </li>
        <li>
          <b>Email :</b> mohnandarizky@gmail.com
        </li>
        <li>
          <b>Sistem Operasi yang digunakan :</b> KDE neon 5.19
        </li>
        <li>
          <b>Akun Gitlab :</b> dananky
        </li>
        <li>
          <b>Akun Telegram :</b> dananky
        </li>
      </ol>
    </div>
  );
};

export default About;
